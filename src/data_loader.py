import os
import pickle
import time

import networkx as nx
import numpy as np
import torch
from scipy import sparse
from sklearn.model_selection import train_test_split

from features_infra.graph_features import GraphFeatures
from layers import AsymmetricGCN, GraphConvolution
from loggers import EmptyLogger


def symmetric_normalization(mx):
    rowsum = np.array(mx.sum(1))
    rowsum[rowsum != 0] **= -0.5
    r_inv = rowsum.flatten()
    # r_inv = np.power(rowsum, -0.5).flatten()
    # r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sparse.diags(r_inv)
    return r_mat_inv.dot(mx).dot(r_mat_inv)  # D^-0.5 * X * D^-0.5


def handle_matrix_concat(mx, is_symetric_normal=True):
    mx += sparse.eye(mx.shape[0])
    mx_t = mx.transpose()
    if is_symetric_normal:
        mx = symmetric_normalization(mx)
        mx_t = symmetric_normalization(mx_t)

    return sparse.vstack([mx, mx_t])  # vstack: below, hstack: near


def handle_matrix_symmetric(mx):
    # build symmetric adjacency matrix
    mx += (mx.T - mx).multiply(mx.T > mx)
    return symmetric_normalization(mx + sparse.eye(mx.shape[0]))


def sparse_mx_to_torch_sparse_tensor(sparse_mx) -> torch.sparse.FloatTensor:
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(np.vstack((sparse_mx.row, sparse_mx.col))).long()
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)


class GraphLoader(object):
    def __init__(self, data_dir, dataset, with_cuda=False, logger=None):
        super(GraphLoader, self).__init__()
        self._logger = EmptyLogger() if logger is None else logger
        self._data_dir = data_dir
        self._dataset = dataset
        self._with_cuda = with_cuda

        self._logger.debug("Loading %s dataset...", self._dataset)
        self._gnx = pickle.load(open(os.path.join(self.dataset_path, "gnx.pkl"), "rb"))
        self._nodes_order = sorted(self._gnx)
        self.layer_model = None
        self._cached = None
        self._train_set = self._test_set = self._train_idx = self._test_idx = None

    # properties' getters and setters
    dataset_path = property(lambda self: os.path.join(self._data_dir, self._dataset), None, None, "")

    def _encode_onehot_gnx(self):  # gnx, nodes_order: list = None):
        nodes_order = self._nodes_order
        if nodes_order is None:
            nodes_order = sorted(self._gnx)
        labels = self._gnx.graph["node_labels"]
        labels_dict = {c: np.identity(len(labels))[i, :] for i, c in enumerate(labels)}
        labels_dict.update({i: labels_dict[c] for i, c in enumerate(labels)})
        return np.array(list(map(lambda n: labels_dict[self._gnx.node[n]['label']], nodes_order)), dtype=np.int32)

    def split_test(self, test_p):
        indexes = range(len(self._nodes_order))
        self._train_set, self._test_set, self._train_idx, self._test_idx = train_test_split(self._nodes_order, indexes,
                                                                                            test_size=test_p,
                                                                                            shuffle=True)
        self._test_set = set(self._test_set)

    def load(self, train_p, features_meta, is_asymmetric=False, feature_path=None):
        if feature_path is None:
            feature_path = self.dataset_path

        # indexes = range(len(self._nodes_order))
        # train_set, test_set, train_idx, idx_test = train_test_split(self._nodes_order, indexes, test_size=1 - train_p,
        #                                                             shuffle=True)
        # val_set, test_set, idx_val, idx_test = train_test_split(test_set, idx_test,
        #                                                         test_size=1 + (val_p / (train_p - 1)),
        #                                                         shuffle=True)
        train_set, val_set, train_idx, val_idx = train_test_split(self._train_set, self._train_idx,
                                                                  test_size=1-train_p, shuffle=True)
        train_set = set(train_set)

        features = GraphFeatures(self._gnx, features_meta, dir_path=feature_path, logger=self._logger)
        features.build(include=train_set)

        feature_mx = features.sparse_matrix(add_ones=True, dtype=np.float32)
        # features.dump()

        labels = self._encode_onehot_gnx()

        adj = nx.adjacency_matrix(self._gnx, nodelist=self._nodes_order).astype(np.float32)

        if is_asymmetric:
            adj, self.layer_model = handle_matrix_concat(adj), AsymmetricGCN
        else:
            adj, self.layer_model = handle_matrix_symmetric(adj), GraphConvolution

        features = torch.FloatTensor(feature_mx.toarray())  # np.array(features.todense()))
        labels = torch.LongTensor(np.where(labels)[1])
        adj = sparse_mx_to_torch_sparse_tensor(adj)
        adj = adj.to_dense()

        train_idx = torch.LongTensor(train_idx)
        val_idx = torch.LongTensor(val_idx)
        idx_test = torch.LongTensor(self._test_idx)

        if self._with_cuda:
            return [x.cuda() for x in [adj, features, labels, train_idx, val_idx, idx_test]]

        return adj, features, labels, train_idx, val_idx, idx_test

    def load_cached(self, *args, **kwargs):
        if self._cached is None:
            self._cached = self.load(*args, **kwargs)
        return self._cached
