import networkx as nx
import numpy as np
from scipy import sparse
import torch

# For loading the feature matrix
import os
import pickle


def encode_onehot(labels):
    classes = set(labels)
    classes_dict = {c: np.identity(len(classes))[i, :] for i, c in
                    enumerate(classes)}
    labels_onehot = np.array(list(map(classes_dict.get, labels)), dtype=np.int32)
    return labels_onehot, {c: int(np.dot(vect, np.linspace(0, 6, 7))) for c, vect in classes_dict.items()}


def encode_onehot_gnx(gnx, nodes_order: list = None):
    if nodes_order is None:
        nodes_order = sorted(gnx)
    labels = gnx.graph["labels"]
    labels_dict = {c: np.identity(len(labels))[i, :] for i, c in enumerate(labels)}
    labels_dict.update({i: labels_dict[c] for i, c in enumerate(labels)})
    return np.array(list(map(lambda n: labels_dict[gnx.node[n]['label']], nodes_order)), dtype=np.int32)


def build_gnx(data_dir):
    idx_features_labels = pickle.load(open(os.path.join(data_dir, "idx_feature_labels.pkl"), "rb"))
    idx = np.array(idx_features_labels[:, 0], dtype=np.int32)
    idx_map = {j: i for i, j in enumerate(idx)}
    edges_unordered = np.genfromtxt("{}{}.cites".format("../data/cora/", "cora"), dtype=np.int32)
    edges = np.array(list(map(idx_map.get, edges_unordered.flatten())), dtype=np.int32).reshape(edges_unordered.shape)

    gnx = nx.DiGraph()
    gnx.add_edges_from(edges_unordered, weight=1.)
    for node_feature in idx_features_labels:
        gnx.node[node_feature[0].astype(np.int32)]['label'] = node_feature[-1]
    pickle.dump(gnx, open(os.path.join(data_dir, "gnx.pkl"), 'wb'))
    return gnx


def symmetric_normalization(mx):
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -0.5).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sparse.diags(r_inv)
    return r_mat_inv.dot(mx).dot(r_mat_inv)  # D^-0.5 * X * D^-0.5


def normalize(mx):
    """Row-normalize sparse matrix
    Dividing each element in the sum of the row
    """
    rowsum = np.array(mx.sum(1))
    r_inv = np.power(rowsum, -1).flatten()
    r_inv[np.isinf(r_inv)] = 0.
    r_mat_inv = sparse.diags(r_inv)
    mx = r_mat_inv.dot(mx)
    return mx


def handle_adj_matrix_concat(adj, is_symetric_normal=True):
    adjt = adj.transpose()
    if is_symetric_normal:
        adj = symmetric_normalization(adj)
        adjt = symmetric_normalization(adjt)

    return np.concatenate([adj, adjt], axis=0)  # 0: below, 1: near


def handle_adj_matrix_symmetric(adj):
    # build symmetric adjacency matrix
    adj += (adj.T - adj).multiply(adj.T > adj)
    return symmetric_normalization(adj + sparse.eye(adj.shape[0]))

    # adj = adj + adj.T.multiply(adj.T > adj) - adj.multiply(adj.T > adj)
    # why not: adj += (adj.T > adj)  ???

def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def sparse_mx_to_torch_sparse_tensor(sparse_mx):
    """Convert a scipy sparse matrix to a torch sparse tensor."""
    sparse_mx = sparse_mx.tocoo().astype(np.float32)
    indices = torch.from_numpy(np.vstack((sparse_mx.row, sparse_mx.col))).long()
    values = torch.from_numpy(sparse_mx.data)
    shape = torch.Size(sparse_mx.shape)
    return torch.sparse.FloatTensor(indices, values, shape)


# Loading our features matrix
def load_features_matrix():
    import featuresList
    from features_calculator import featuresCalculator

    basedir = os.path.expanduser(r"~/git/network-analysis/data/directed/cora")
    file_in = os.path.join(basedir, "input", "cora.txt")
    output_dir = os.path.join(basedir, "features")
    directory_tags_path = os.path.join(basedir, "tags", "")

    calculator = featuresCalculator()
    features_list = featuresList.featuresList(directed=True, analysisType='nodes').getFeatures()
    gnx, map_features = calculator.calculateFeatures(features_list, file_in, output_dir, directed=True,
                                                     analysisType='nodes')
    return gnx
    tags_loader = TagsLoader(directory_tags_path, ["cora_tags"])
    tags_loader.Load()

    vertex_to_tags = tags_loader.calssification_to_vertex_to_tag['cora_tags']
    all_features = FeaturesMatrix.build_matrix_with_tags(gnx, map_features, vertex_to_tags, zscoring=True)
    return all_features[0]

    # def load_data(data_dir="../data/cora/", dataset="cora"):
    # # TODO: delete
    # def l(p):
    #     bp = os.path.join("/home/orion/git/data/citeseer/bla", p)
    #     return pickle.load(open(bp + ".pkl", "rb"))
    #
    #
    # def get_data_args(num_entries, config):
    #     train_per = config.getfloat("train_percent")
    #     validate_per = config.getfloat("validate_percent")
    #     num_train = int(train_per * num_entries)
    #     num_val = int(validate_per * num_entries)
    #     return num_train, num_val, num_entries - num_train - num_val

    # num_train, num_val, num_test = get_data_args(len(gnx), config)
    # train_set = set(random.sample(nodes, num_train))
    # remaining_nodes = nodes.difference(train_set)
    # val_set = set(random.sample(nodes.difference(train_set), num_val))
    # test_set = nodes.difference(train_set).difference(val_set)

    # idx_train = range(num_train)
    # idx_val = range(num_train, num_train + num_val)
    # idx_test = range(num_train + num_val, num_train + num_val + num_test)

    # data_dir = r"/home/orion/git/data/cora"
    # build_gnx(data_dir)
    # idx_features_labels = np.genfromtxt("{}{}.content".format(path, dataset), dtype=np.dtype(str))
    # idx_features_labels = pickle.load(open(os.path.join(dataset_path, "idx_feature_labels.pkl"), "rb"))
    # features = sparse.csr_matrix(idx_features_labels[:, 1:-1], dtype=np.float32)
    # idx_features_labels[:, -1])

    # myfeatures = load_features_matrix()
    # myfeatures = pickle.load(open(os.path.join(data_dir, "cora_topo_mx.pkl"), "rb"))

    # features = sparse.csr_matrix(myfeatures, dtype=np.float32)

    # build graph:
    #   * Mapping all document IDs to their indexes
    #   * edges_unordered: loading the edges file (*.cites)
    #   * edges: translating the edges according to the mapping
    #   * adj: adjacency matrix (square matrix)
    # idx = sorted(np.array(idx_features_labels[:, 0], dtype=np.int32))
    # idx_map = {j: i for i, j in enumerate(idx)}
    # edges_unordered = np.genfromtxt("{}{}.cites".format(path, dataset), dtype=np.int32)
    # edges = np.array(list(map(idx_map.get, edges_unordered.flatten())), dtype=np.int32).reshape(edges_unordered.shape)
    # adj = sparse.coo_matrix((np.ones(edges.shape[0]), (edges[:, 0], edges[:, 1])),
    #                     shape=(labels.shape[0], labels.shape[0]), dtype=np.float32)

    # features = normalize(features)

    # idx_train = range(800)  # 140
    # idx_val = range(800, 1600)  # 200, 500
    # idx_test = range(1600, 2200)  # 500, 1500
