import tensorflow as tf


if __name__ == "__main__":
    writer1 = tf.summary.FileWriter("./logs/b1")
    writer2 = tf.summary.FileWriter("./logs/b2")
    # tf.summary.merge_all()
    # tf.summary.merge([writer1, writer2])

    # tf.summary.scalar("loss", log_var)

    # def scalar_summary(self, tag, value, step):
    #     """Log a scalar variable."""
    summary = tf.Summary(value=[tf.Summary.Value(tag="bla", simple_value=2)])
    writer1.add_summary(summary, 1)
    summary = tf.Summary(value=[tf.Summary.Value(tag="bla", simple_value=3)])
    writer2.add_summary(summary, 1)
    print("Bla")
