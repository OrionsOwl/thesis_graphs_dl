# from __future__ import division

# noinspection PyCompatibility
import configparser
import json
import logging
import os
import shutil
import time
from itertools import product

import numpy as np
import pandas
import torch
import torch.nn.functional as functional
import torch.optim as optim
from torch.autograd import Variable

# PYGCN
from data_loader import GraphLoader
from features_algorithms.vertices.neighbor_nodes_histogram import nth_neighbor_calculator
from features_infra.feature_calculators import FeatureMeta
from loggers import PrintLogger, multi_logger, FileLogger
from models import GCN

# PyTorch utils
# from torchsample.modules import ModuleTrainer
# from torchsample.callbacks import EarlyStopping
# from torchsample.regularizers import L1Regularizer, L2Regularizer
# from torchsample.metrics import CategoricalAccuracy

# IterRes = namedtuple("IterationResults", ("train", "val"))
# SetRes = namedtuple("SetResults", ("loss", "acc"))
from tensor_board_logger import TensorBoardLogger


def accuracy(output, labels):
    preds = output.max(1)[1].type_as(labels)
    correct = preds.eq(labels).double()
    correct = correct.sum()
    return correct / len(labels)


def load_arguments(config_path):
    config = configparser.ConfigParser(inline_comment_prefixes=";")
    config.read_file(open(config_path))

    with_cuda = not config["TRAIN"]["no_cuda"] and torch.cuda.is_available()
    config.set("TRAIN", "cuda", str(with_cuda))

    if "seed" not in config["TRAIN"]:
        return config

    seed = config.getint("TRAIN", "seed")
    np.random.seed(seed)
    torch.manual_seed(seed)

    if with_cuda:
        torch.cuda.manual_seed(seed)

    return config


def to_np(x):
    return x.data.cpu().numpy()


def to_var(x):
    if torch.cuda.is_available():
        x = x.cuda()
    return Variable(x)


NEIGHBOR_FEATURES = {
    "first_neighbor_histogram": FeatureMeta(nth_neighbor_calculator(1), {"fnh", "first_neighbor"}),
    "second_neighbor_histogram": FeatureMeta(nth_neighbor_calculator(2), {"snh", "second_neighbor"}),
}


class ModelRunner(object):
    def __init__(self, conf, logger=None):
        super(ModelRunner, self).__init__()
        if logger is None:
            logger = PrintLogger()
        self._logger = logger
        # self._products_path = products_path

        self.data_loader = GraphLoader(conf["base_dir"], conf["dataset"], with_cuda=conf["cuda"], logger=logger)

    def run_model(self, name, conf, products_path, tb_logger):
        feature_path = os.path.join(products_path, "features")
        # Load data
        adj, features, labels, idx_train, idx_val, idx_test = self.data_loader.load(
            conf["train_p"], NEIGHBOR_FEATURES, is_asymmetric=conf["asymm"], feature_path=feature_path)

        model_conf = {"nfeatures": features.shape[1], "nclasses": labels.max() + 1,
                      "dropout": conf["dropout"], "activation_func": conf["act_func"]}

        # model and optimizer
        model = GCN(model_conf, layer_model=self.data_loader.layer_model)
        optimizer = optim.Adam(model.parameters(), lr=conf["learning_rate"], weight_decay=conf["weight_decay"])

        if conf["cuda"]:
            model.cuda()

        features, adj, labels = Variable(features), Variable(adj), Variable(labels)

        epoch = 1
        best_params = None
        window = conf["early_stop_window"]

        self._logger.info("Running: train_p: %.4f, val_p: %.4f", conf["train_p"], conf["val_p"])

        results = []
        while True:
            t = time.time()
            model.train(mode=True)
            optimizer.zero_grad()

            output = model(features, adj)
            loss_train = functional.nll_loss(output[idx_train], labels[idx_train])
            loss_train.backward()
            optimizer.step()
            acc_train = accuracy(output[idx_train], labels[idx_train])
            loss_reg = conf["weight_decay"] * torch.sqrt(
                sum(torch.sum(x ** 2) for x in model.parameters()))  # L2 regularization

            if not conf["fast_mode"]:
                # Evaluate validation set performance separately,
                # deactivates dropout during validation run.
                model.eval()
                output = model(features, adj)

            loss_val = functional.nll_loss(output[idx_val], labels[idx_val])
            acc_val = accuracy(output[idx_val], labels[idx_val])

            # ============ TensorBoard logging ============#
            # (1) Log the scalar values
            info = {
                'train_loss': loss_train.data[0],
                'train_reg': loss_reg.data[0],
                'train_accuracy': acc_train.data[0],
                'val_loss': loss_val.data[0],
                'val_accuracy': acc_val.data[0],
            }

            for tag, value in info.items():
                tag = "%s_%s" % (name, tag)
                tb_logger.scalar_summary(tag, value, epoch + 1)

            # (2) Log values and gradients of the parameters (histogram)
            for tag, value in model.named_parameters():
                tag = tag.replace('.', '/')
                tag = "%s_%s" % (name, tag)
                tb_logger.histo_summary(tag, to_np(value), epoch + 1)
                tb_logger.histo_summary(tag + '/grad', to_np(value.grad), epoch + 1)
            # ============ TensorBoard logging ============#

            curr_res = {"loss_train": loss_train.data[0], "acc_train": acc_train.data[0],
                        "loss_val": loss_val.data[0], "acc_val": acc_val.data[0], "epoch": epoch,
                        "loss_reg": loss_reg.data[0]}
            results.append(curr_res)
            # self._logger.debug(", ".join("%s: %.4f" % (name, val) for name, val in curr_res.items()))
            self._logger.debug("epoch: %04d, loss_train %.4f, acc_train: %.4f, loss_val: %.4f, acc_val: %.4f, "
                               "loss_reg: %.4f, time: %.4fs", epoch, loss_train.data[0], acc_train.data[0],
                               loss_val.data[0],
                               acc_val.data[0], curr_res["loss_reg"], time.time() - t)

            window -= 1

            if best_params is None or best_params["loss_val"].data[0] > loss_val.data[0]:
                best_params = {
                    'epoch': epoch + 1,
                    'loss_val': loss_val,
                    'acc_val': acc_val,
                    'state_dict': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                }
                window = conf["early_stop_window"]

            if 0 == window:
                break

            epoch += 1
        # return IterRes(SetRes(loss_train.data[0], acc_train.data[0]), SetRes(loss_val.data[0], acc_val.data[0]))

        # "train_p", "val_p", "learning_rate", "weight_decay", "dropout", "early_stop_window", "epochs"
        conf["epochs"] = epoch
        act_func = conf.pop("act_func")
        conf["act_func"] = act_func.__name__
        json.dump(conf, open(os.path.join(products_path, "parameters.json"), "w"))
        conf["act_func"] = act_func
        model.load_state_dict(best_params["state_dict"])
        optimizer.load_state_dict(best_params["optimizer"])

        # Test model
        model.eval()
        output = model(features, adj)
        best_params["loss_test"] = functional.nll_loss(output[idx_test], labels[idx_test])
        best_params["acc_test"] = accuracy(output[idx_test], labels[idx_test])
        best_params["arguments"] = conf

        return best_params


def parse_config(conf_path):
    def parse_arg(val):
        try:
            val = int(val)
        except ValueError:
            try:
                val = float(val)
            except ValueError:
                if val.lower() in ["no", "false", "n", "nope"]:
                    val = False
                elif val.lower() in ["yes", "true", "y"]:
                    val = True
        return val

    config = load_arguments(conf_path)
    return {k2: parse_arg(v2) for _, v1 in config.items() for k2, v2 in v1.items()}


def log_prev_results(base_tb, dataset_path):
    tb_log1 = TensorBoardLogger(os.path.join(base_tb, "keren"))
    tb_log2 = TensorBoardLogger(os.path.join(base_tb, "kipf"))

    df = pandas.read_csv(os.path.join(dataset_path, "results.csv"))
    for i, row in df.iterrows():
        tp = row.train_p * 100
        tb_log1.scalar_summary("test_acc", row.keren, tp)
        tb_log2.scalar_summary("test_acc", row.kipf, tp)
        # print("Log %.4f keren: %.4f kipf: %.4f" % (row.train_p, row.keren, row.kipf))

    # df = pandas.read_csv("/home/orion/git/data/cora/results.df")
    # for i, row in df.iterrows():
    #     tp = row.train_p * 100
    #     print(tp)
    #     tb_log1.scalar_summary("bla", row.keren, tp)
    #     tb_log2.scalar_summary("bla", row.kipf, tp)


def normal_main(runner, config, logger, products_path):
    # run_path = lambda name: os.path.join(products_path, name + time.strftime("_%H_%M_%S"))
    num_samples = 3

    config["act_func"] = functional.relu
    # config.update({"dropout": do, "learning_rate": lr, "weight_decay": wd, "act_func": af, "asymm": True})

    base_tb = config["base_tb"] # "./tb_logs3"
    base_tb = os.path.join(base_tb, config["dataset"])

    if os.path.exists(base_tb):
        shutil.rmtree(base_tb)

    tb_log = TensorBoardLogger(os.path.join(base_tb, "idan"))

    log_prev_results(base_tb, runner.data_loader.dataset_path)
    
    res = []
    wds = {train_p: 0.035 for train_p in [1] + list(range(5, 100, 10))}
    wds.update({1: 0.05, 5: 0.05, 15: 0.04})

    for train_p in [1] + list(range(5, 100, 10)):
        config["weight_decay"] = wds[train_p]

        name = str(train_p)
        config["val_p"] = config["test_p"] = val_p = (100 - train_p) / 2
        config["train_p"] = train_p / (val_p + train_p)

        runner.data_loader.split_test(config["test_p"] / 100)
        cur_prod_path = get_new_path(products_path)

        params = [runner.run_model(name, config, cur_prod_path,
                                   TensorBoardLogger(os.path.join(base_tb, "%d_%d" % (train_p, i))))
                  for i in range(num_samples)]
        avg_acc = np.average(list(map(lambda x: x["acc_test"].data[0], params)))
        avg_loss = np.average(list(map(lambda x: x["loss_test"].data[0], params)))
        tb_log.scalar_summary("test_loss", avg_loss, train_p)
        tb_log.scalar_summary("test_acc", avg_acc, train_p)

        logger.info("Test results: train_p: %02d%c, acc: %.4f, loss: %.4f", train_p, '%', avg_acc, avg_loss)
        res.append((train_p, avg_acc, avg_loss))
        # logger.debug(dict(best_params["arguments"]))

    json.dump(res, open(os.path.join(products_path, "results.JSON"), "w"))


def get_new_path(base_path):
    idx = 1
    base_path = cur_path = os.path.join(base_path, time.strftime("_%H_%M_%S"))
    while os.path.exists(cur_path):
        cur_path = "%s_%s" % (base_path, idx)
        idx += 1
    os.makedirs(cur_path)
    return cur_path


def find_args(runner, config, logger, products_path):
    num_samples = 4
    res = []
    train_p = 1 / 100
    test_p = val_p = (1 - train_p) / 2
    train_p = train_p / (val_p + train_p)
    config["train_p"] = train_p

    dropouts = [0.2, 0.4, 0.5, 0.7]
    lrs = [0.001, 0.01, 0.05, 0.1]
    decays = [0.0005, 0.005, 0.01]
    acts = [functional.relu]  # , functional.sigmoid, functional.tanh]  # , functional.linear]

    # ok - 0.5 0.05 5e-4
    # dropouts = np.arange(0, 0.9, 0.1)  # [0.2, 0.4, 0.7]
    # lrs = [0.001, 0.005, 0.02, 0.03, 0.05, 0.06, 0.08, 0.1, 0.5]  # [0.05, 0.03, 0.08]
    # decays = [0.0002, 0.0004, 0.0006, 0.0008, 0.003, 0.006, 0.009, 0.03, 0.06, 0.09]  # [0.005, 0.0002, 0.0008]

    perms = [
        # (0.5, 0.0005, 0.05),  # 3783
        # (0.5, 0.0005, 0.001),  # 3783
        (0.5, 0.0005, 0.05),  # 3783
        # (0.7, 0.005, 0.03),  # 4143
        # (0.7, 0.0008, 0.05),  # 3441
        # (0.4, 0.0002, 0.08),  # 3459
        # (0.7, 0.0002, 0.08),  # 3941
        # (0.7, 0.0002, 0.03),  # 3612
        # (0.2, 0.0002, 0.08),  # 3813

        # (0.4, 0.0008, 0.08),  # 3400
        # (0.7, 0.0002, 0.05),  # 2902
        # (0.7, 0.0002, 0.03),  # 2902
        # (0.7, 0.0008, 0.03),
    ]
    # dropouts = [0.5, 0.2]  # , 0.4, 0.7]
    # lrs = [0.05, 0.03]  # , 0.08]
    # decays = [0.0002, 0.0008]
    runner.data_loader.split_test(test_p)

    params = [("af", acts), ("wd", decays), ("do", dropouts), ("lr", lrs)]
    indexes = {name: i for i, (name, _) in enumerate(params)}
    # for pars in product(*[x[1] for x in params]):
    #     af, wd, do, lr = [pars[indexes[x]] for x in ["af", "wd", "do", "lr"]]
    af = acts[0]
    for do, wd, lr in perms:
        config.update({"dropout": do, "learning_rate": lr, "weight_decay": wd, "act_func": af, "asymm": True})
        logger.info("ActFunc: %s, Dropout: %s, WeightDecay: %s, LearningRate: %s", af.__name__, do, wd, lr)
        cur_prod_path = get_new_path(products_path)
        params = [runner.run_model(config, cur_prod_path) for _ in range(num_samples)]
        avg_acc = np.average(list(map(lambda x: x["acc_test"].data[0], params)))
        avg_loss = np.average(list(map(lambda x: x["loss_test"].data[0], params)))
        logger.info("******* Test results: do: %s, wd: %s, lr: %s ### loss: %.4f acc: %.4f ###", do, wd, lr, avg_loss,
                    avg_acc)
        res.append((do, lr, wd, af.__name__, avg_acc, avg_loss))
    print("Bla")
    json.dump(res, open(os.path.join(products_path, "results.json"), "w"))


def main():
    config = parse_config("arguments.cfg")

    products_path = os.path.join("products", config["dataset"], time.strftime("%Y_%m_%d_%H_%M_%S"))
    os.makedirs(products_path)

    logger = multi_logger(
        [PrintLogger(level=logging.INFO), FileLogger("%s_ptrain_acc" % config["dataset"], path=products_path)])
    runner = ModelRunner(config, logger=logger)

    # find_args(runner, config, logger, products_path)
    normal_main(runner, config, logger, products_path)


if __name__ == "__main__":
    main()

########################################################################################################################
########################################################################################################################
# def main():
#     config = parse_args()
#
#     runner = ModelRunner(config)
#
#     # Train model
#     t_total = time.time()
#     res = []
#     epoch = 0
#     while True:
#         epoch += 1
#         # for epoch in range(config.epochs):
#         res.append(runner.train(epoch))
#         if len(res) < 5:
#             continue
#         accs = set(map(lambda ret: ret.train.acc, res))
#         if max(accs) - min(accs) < 0.005:
#             break
#         res.pop(0)

# "".format(epoch + 1),
# 'loss_train: {:.4f}'.format(loss_train.data[0]),
# 'acc_train: {:.4f}'.format(acc_train.data[0]),
# 'loss_val: {:.4f}'.format(loss_val.data[0]),
# 'acc_val: {:.4f}'.format(acc_val.data[0]),
# 'time: {:.4f}s'.format(time.time() - t))
# "loss= {:.4f}".format(loss_test.data[0]),
# "accuracy= {:.4f}".format(acc_test.data[0]))


# def sample_early_stop(self):
#     # Defining the PyTorch utilities
#     trainer = ModuleTrainer(self._model)
#     callbacks = [EarlyStopping(monitor='val_loss', patience=5)]
#     trainer.set_callbacks(callbacks)
#     #
#     regularizers = [L1Regularizer(scale=1e-4, module_filter='*conv*')]
#     trainer.set_regularizers(regularizers)
#
#     callbacks = [EarlyStopping(monitor="val_loss", patience=10)]
#                  # ReduceLROnPlateau(factor=0.5, patience=5)]
#     # regularizers = [L1Regularizer(scale=1e-3, module_filter='conv*'),
#     regularizers = [L2Regularizer(scale=1e-5, module_filter='conv*')]
#     # constraints = [UnitNorm(frequency=3, unit='batch', module_filter='fc*')]
#     # initializers = [XavierUniform(bias=False, module_filter='fc*')]
#     metrics = [CategoricalAccuracy()]
#
#     # self._optimizer.zero_grad()
#     trainer.compile(loss='nll_loss',
#                     optimizer=self._optimizer,
#                     regularizers=regularizers,
#                     callbacks=callbacks)
#                     # constraints=constraints,
#                     # initializers=initializers,
#                     # metrics=metrics)
#
#     trainer.fit((self._features, self._adj), self._labels[self._idx_train],
#                 val_data=(x_test, y_test),
#                 num_epoch=20,
#                 batch_size=128,
#                 verbose=1)
#
#     trainer.fit(x_train, y_train,
#                 val_data=(x_test, y_test),


# def run_model(self):
#     t_total = time.time()
#     # Train model
#     for epoch in range(self._conf.getint("epochs")):
#         self.train(epoch)
#
#     self._logger.info("Optimization Finished!")
#     self._logger.info("Total time elapsed: %.4fs", time.time() - t_total)
#
#     # Testing
#     return self._test()
#
# def run_early_stop(self, window_size):
#     updated_epoch = min_val = None
#
#     epoch = 1
#
#     t_total = time.time()
#     while True:
#         self.train(epoch)
#         if min_val is None or min_val > 0:
#             min_val = 0
#             updated_epoch = epoch
#
#         if epoch - updated_epoch >= window_size:
#             break
#         epoch += 1
#
#     self._logger.info("Optimization Finished!")
#     self._logger.info("Total time elapsed: %2.3f", time.time() - t_total)
