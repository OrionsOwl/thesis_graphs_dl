import torch.nn as nn
import torch.nn.functional as functional

from layers import GraphConvolution


class GCN(nn.Module):
    # model_conf = {"nfeatures": 3, "nclasses": 5, "dropout": 0.3, "activation_func": functional.relu}
    def __init__(self, conf, layer_model=None):
        super(GCN, self).__init__()
        # hidden_layers = [conf["nfeatures"], 100, 50, 20, conf["nclasses"]]
        hidden_layers = [conf["nfeatures"], 70, 35, conf["nclasses"]]
        # hidden_layers = [conf["nfeatures"], 100, 70, 35, 15, conf["nclasses"]]
        if layer_model is None:
            layer_model = GraphConvolution

        self._layers = nn.ModuleList([layer_model(first, second)
                                     for first, second in zip(hidden_layers[:-1], hidden_layers[1:])])
        self._dropout = conf.get("dropout", 0)
        self._activation_func = conf.get("activation_func", functional.relu)

    def forward_dep(self, x, adj):
        x = functional.relu(self.gc1(x, adj))
        x = functional.dropout(x, self.dropout, training=self.training)
        x = self.gc2(x, adj)
        return functional.log_softmax(x)

    def forward(self, x, adj):
        layers = list(self._layers)
        for layer in layers[:-1]:
            x = self._activation_func(layer(x, adj))
            x = functional.dropout(x, self._dropout, training=self.training)
        x = layers[-1](x, adj)
        return functional.log_softmax(x, dim=1)
